import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String errorMessage = 'Epic sadface: Sorry, this user has been locked out.'

TestData dataUserForLogin = findTestData('Data Files/AllUserData')

WebUI.openBrowser('https://www.saucedemo.com/', FailureHandling.CONTINUE_ON_FAILURE)

for (int rowData = 1; rowData <= dataUserForLogin.getRowNumbers(); rowData++) {
	
    String username = dataUserForLogin.getValue(1, rowData)
    String password = dataUserForLogin.getValue(2, rowData)

    WebUI.setText(findTestObject('sauceLabsPage/loginPage/inputUsername'), username, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('sauceLabsPage/loginPage/inputPassword'), password, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('sauceLabsPage/loginPage/loginButton'), FailureHandling.CONTINUE_ON_FAILURE)

    if (username == 'locked_out_user') {
        WebUI.verifyElementPresent(findTestObject('Object Repository/sauceLabsPage/loginPage/alertError'), 5)

        WebUI.verifyElementText(findTestObject('Object Repository/sauceLabsPage/loginPage/alertError'), errorMessage)
    } else {
        WebUI.verifyElementPresent(findTestObject('Object Repository/sauceLabsPage/homePage/navigationHeader'), 5)

        WebUI.click(findTestObject('Object Repository/sauceLabsPage/homePage/hamburgerMenu'))

        WebUI.click(findTestObject('Object Repository/sauceLabsPage/homePage/logoutMenu'))

        WebUI.verifyElementPresent(findTestObject('Object Repository/sauceLabsPage/loginPage/inputUsername'), 5)
    }
}

